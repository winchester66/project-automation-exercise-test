package com.testesautomatizados.steps;

import com.testesautomatizados.core.selenium.NavigatorActions;
import com.testesautomatizados.pageobject.HomePO;
import com.testesautomatizados.pageobject.LoginPO;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.jupiter.api.Assertions;

public class LoginSteps {

    private NavigatorActions navigator = new NavigatorActions();

    @Quando("realizo login com email {string} e senha {string}")
    public void realizoLoginComEmailSenha(String email, String senha) {
        navigator.click(HomePO.BTN_LOGIN_SINGUP);
        navigator.sendKeys(LoginPO.INPUT_LOGIN_EMAIL, email);
        navigator.sendKeys(LoginPO.INPUT_LOGIN_PASSWORD, senha);
        navigator.click(LoginPO.BTN_SUBMIT);
    }

    @Então("valido que login foi realizado com sucesso")
    public void validoLoginComSucesso() throws InterruptedException {
        navigator.takeScreenShot();
        Assertions.assertEquals(true, navigator.verifyPresenceOfElement(HomePO.LBL_VALIDATION_LOGIN), "Login não realizado!");
    }
}
