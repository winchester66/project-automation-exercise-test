# language: pt
# Author: Carlos Moreira
# Date: 13/07/2023

@all @criarUsuario
Funcionalidade: Cadastro de Usuário

  Cenário: Criar usuário com sucesso
    Dado que estou na pagina principal
    Quando sigo para a tela de criação de conta inserindo nome e email
    E insiro os dados para criação de conta
    Então valido que conta foi criada com sucesso