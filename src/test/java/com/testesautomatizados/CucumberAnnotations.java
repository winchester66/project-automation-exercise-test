package com.testesautomatizados;

import com.testesautomatizados.core.selenium.Navigator;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;

public class CucumberAnnotations {
	
	@After
	public void afterScenario()  {
		Navigator.closeDriver();
	}
}
