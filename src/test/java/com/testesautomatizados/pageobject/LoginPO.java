package com.testesautomatizados.pageobject;

import org.openqa.selenium.By;

public class LoginPO {

	public static By INPUT_LOGIN_EMAIL = By.xpath("//input[contains(@data-qa,'login-email')]");
	public static By INPUT_LOGIN_PASSWORD = By.xpath("//input[contains(@data-qa,'login-password')]");
	public static By BTN_SUBMIT = By.xpath("//button[contains(@data-qa,'login-button')]");
	public static By INPUT_SINGUP_NAME = By.xpath("//input[@data-qa='signup-name']");
	public static By INPUT_SINGUP_EMAIL = By.xpath("//input[@data-qa='signup-email']");
	public static By BTN_SINGUP = By.xpath("//button[@data-qa='signup-button']");


}
