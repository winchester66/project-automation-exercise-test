package com.testesautomatizados.pageobject;

import org.openqa.selenium.By;

public class CheckoutPO {

	public static By BTN_PROCEED_CHECKOUT = By.xpath("//a[contains(text(),'Proceed To Checkout')]");
	public static By BTN_PLACE_ORDER = By.xpath("//a[contains(text(),'Place Order')]");
	public static By LBL_NAME_CARD = By.xpath("//input[contains(@data-qa,'name-on-card')]");
	public static By LBL_CARD_NUMBER = By.xpath("//input[contains(@data-qa,'card-number')]");
	public static By LBL_CVC = By.xpath("//input[contains(@data-qa,'cvc')]");
	public static By LBL_EXPIRATION_MONTH = By.xpath("//input[contains(@data-qa,'expiry-month')]");
	public static By LBL_EXPIRATION_YEAR = By.xpath("//input[contains(@data-qa,'expiry-year')]");
	public static By BTN_CONFIRM_ORDER = By.xpath("//button[contains(@data-qa,'pay-button')]");

}
