package com.testesautomatizados.steps;

import com.github.javafaker.Faker;
import com.testesautomatizados.core.selenium.NavigatorActions;
import com.testesautomatizados.pageobject.AccountInformationPO;
import com.testesautomatizados.pageobject.HomePO;
import com.testesautomatizados.pageobject.LoginPO;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.jupiter.api.Assertions;

public class CriarUsuarioSteps {

    private NavigatorActions navigator = new NavigatorActions();
    Faker faker = new Faker();

    @Quando("sigo para a tela de criação de conta inserindo nome e email")
    public void sigoParaTelaCriacaoConta() {
        navigator.click(HomePO.BTN_LOGIN_SINGUP);
        navigator.waitPresenceOfElement(LoginPO.INPUT_SINGUP_NAME);
        navigator.sendKeys(LoginPO.INPUT_SINGUP_NAME, String.format("%s %s", faker.address().firstName(), faker.address().lastName()));
        navigator.sendKeys(LoginPO.INPUT_SINGUP_EMAIL, faker.internet().emailAddress());
        navigator.takeScreenShot();

        navigator.click(LoginPO.BTN_SINGUP);
    }

    @Quando("insiro os dados para criação de conta")
    public void insiroDadosParaCriarConta() {
        navigator.waitPresenceOfElement(AccountInformationPO.CHK_MALE);
        navigator.click(AccountInformationPO.CHK_MALE);
        navigator.sendKeys(AccountInformationPO.INPUT_PASSWORD, faker.internet().password());
        navigator.selectDropDown(AccountInformationPO.SLCT_DAYS, "1");
        navigator.selectDropDown(AccountInformationPO.SLCT_MONTHS, "January");
        navigator.selectDropDown(AccountInformationPO.SLCT_YEAR, "2003");
        navigator.sendKeys(AccountInformationPO.INPUT_FIRST_NAME, faker.address().firstName());
        navigator.sendKeys(AccountInformationPO.INPUT_LAST_NAME, faker.address().lastName());
        navigator.sendKeys(AccountInformationPO.INPUT_COMPANY, faker.company().name());
        navigator.sendKeys(AccountInformationPO.INPUT_ADDRESS_1, faker.address().fullAddress());
        navigator.selectDropDown(AccountInformationPO.SLCT_COUNTRY, "Canada");
        navigator.sendKeys(AccountInformationPO.INPUT_STATE, faker.address().state());
        navigator.sendKeys(AccountInformationPO.INPUT_CITY, faker.address().city());
        navigator.sendKeys(AccountInformationPO.INPUT_ZIPCODE, faker.address().zipCode());
        navigator.sendKeys(AccountInformationPO.INPUT_MOBILE_NUMBER, faker.phoneNumber().cellPhone());
        navigator.takeScreenShot();

        navigator.click(AccountInformationPO.BTN_CREATE_ACCOUNT);
    }

    @Então("valido que conta foi criada com sucesso")
    public void validoCriacaoDeContaComSucesso() throws InterruptedException {
        navigator.takeScreenShot();
        Assertions.assertEquals(true, navigator.verifyPresenceOfElement(AccountInformationPO.TXT_ACCOUNT_CREATED), "Erro ao validar criação de conta!");
    }
}
