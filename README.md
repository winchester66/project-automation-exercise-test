# Projeto de Teste Automatizados
- Site utilizado para teste: https://automationexercise.com/

## Finalidade
- [X] Testar funcionalidade de Login
- [X] Testar funcionalidade de Criação de Conta
- [X] Testar funcionalidade de Comprar Produtos

## Ferramentas e Tecnologias Utilizadas

- [X] [Java]
- [X] [Gherkin]
- [X] [Selenium]
- [X] [JavaFaker]
- [X] [Cucumber]
- [X] [IntelliJ] - Para codar

## Como executar os testes

- Em seu terminal execute o seguinte comando: mvn clean test

## Evidências

- Após rodar os testes será gerado na pasta raiz a seguinte pasta: cucumber-reports. Nela é possível visualizar um HTML contendo as evidências dos últimos testes

## Autor 

- Carlos Moreira 
- Linkedin: https://www.linkedin.com/in/carlos-moreira-887963122/
- Whatsapp: (14)99818-4411

Qualquer dúvida é só me chamar!
