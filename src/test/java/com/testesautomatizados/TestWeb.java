package com.testesautomatizados;

import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PROPERTY_NAME;

import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;

import com.testesautomatizados.core.util.ScenarioUtil;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("/")
@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty,html:cucumber-reports/cucumber.html")
@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "com.testesautomatizados")
@ConfigurationParameter(key = io.cucumber.junit.platform.engine.Constants.ANSI_COLORS_DISABLED_PROPERTY_NAME, value = "true")
@IncludeTags("all")

public class TestWeb {

	@Before
	    public void before(Scenario scenario) {
	       ScenarioUtil.setScenario(scenario);
	    }
	
}
