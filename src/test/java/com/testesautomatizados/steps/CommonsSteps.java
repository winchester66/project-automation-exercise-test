package com.testesautomatizados.steps;

import com.testesautomatizados.core.selenium.Navigator;
import com.testesautomatizados.core.selenium.NavigatorActions;
import com.testesautomatizados.pageobject.HomePO;
import io.cucumber.java.pt.Dado;

public class CommonsSteps {

	NavigatorActions navigator = new NavigatorActions();

	@Dado("que estou na pagina principal")
	public void que_estou_na_pagina_principal() throws Exception {
		Navigator.startBrowser("https://automationexercise.com/");
		navigator.waitPresenceOfElement(HomePO.IMG_HOME);
	}
}
