package com.testesautomatizados.core.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NavigatorActions {

    private WebDriver webdriver = Navigator.getDriver();

    public void click(By element) {
        webdriver.findElement(element).click();
    }

    public void sendKeys(By element, String texto) {
        webdriver.findElement(element).sendKeys(texto);
    }

    public void selectDropDown(By element, String option) {
        WebElement comboboxElement = webdriver.findElement(element);
        comboboxElement.click();
        Select combobox = new Select(comboboxElement);
        combobox.selectByVisibleText(option);
    }

    public void waitPresenceOfElement(By element) {
        WebDriverWait wait = new WebDriverWait(Navigator.getDriver(), 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public Boolean verifyPresenceOfElement(By element) {
        try {
            return webdriver.findElement(element).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickAddProduct(String product) {
        webdriver.findElement(By.xpath("(//p[contains(text(),'" + product + "')]/../a)[1]")).click();
    }

    public void closeAds() {
        try {
            webdriver.switchTo().frame(webdriver.findElement(By.xpath("//iframe[contains(@style,'visibility: visible')]")).getAttribute("id"));
            webdriver.switchTo().frame("ad_iframe");
            webdriver.findElement(By.id("dismiss-button")).click();
            webdriver.switchTo().parentFrame();

            System.out.println("Anúncio exibido e fechado com sucesso!");
        } catch (Exception e) {
            System.out.println("Anúncio não exibido!");
        }
    }

    public void takeScreenShot() {
        Navigator.takeScreenshot();
    }

}
