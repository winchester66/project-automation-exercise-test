package com.testesautomatizados.pageobject;

import org.openqa.selenium.By;

public class AccountInformationPO {

	public static By CHK_MALE = By.id("id_gender1");
	public static By INPUT_PASSWORD = By.id("password");
	public static By INPUT_FIRST_NAME = By.id("first_name");
	public static By INPUT_LAST_NAME = By.id("last_name");
	public static By INPUT_COMPANY = By.id("company");
	public static By INPUT_ADDRESS_1 = By.id("address1");
	public static By INPUT_STATE = By.id("state");
	public static By INPUT_CITY = By.id("city");
	public static By INPUT_ZIPCODE = By.id("zipcode");
	public static By INPUT_MOBILE_NUMBER = By.id("mobile_number");
	public static By BTN_CREATE_ACCOUNT = By.xpath("//*[@data-qa='create-account']");
	public static By SLCT_DAYS = By.id("days");
	public static By SLCT_MONTHS = By.id("months");
	public static By SLCT_YEAR = By.id("years");
	public static By SLCT_COUNTRY = By.cssSelector("#country");
	public static By TXT_ACCOUNT_CREATED = By.xpath("//*[contains(text(),'Account Created!')]");
}
