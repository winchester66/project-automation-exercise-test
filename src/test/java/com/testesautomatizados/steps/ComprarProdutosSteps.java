package com.testesautomatizados.steps;

import com.testesautomatizados.core.selenium.NavigatorActions;
import com.testesautomatizados.pageobject.CheckoutPO;
import com.testesautomatizados.pageobject.HomePO;
import com.testesautomatizados.pageobject.OrderCompletePO;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.jupiter.api.Assertions;

import java.util.List;

public class ComprarProdutosSteps {

    private NavigatorActions navigator = new NavigatorActions();

    @Quando("adiciono os produtos no carrinho")
    public void adicionoProdutoNoCarrinho(DataTable table) {
        List<List<String>> rows = table.asLists(String.class);

        for (List<String> columns : rows) {
            Integer quantidadeProdutos = Integer.parseInt(columns.get(1));
            for (int i = 0; i < quantidadeProdutos; i++) {
                navigator.clickAddProduct(columns.get(0));
                navigator.click(HomePO.BTN_CONTINUE_SHOPPING);
            }
        }
    }

    @E("sigo para a tela de pagamento")
    public void sigoParaTelaDePagamentos() throws InterruptedException {
        navigator.click(HomePO.LNK_VIEW_CART);
        navigator.takeScreenShot();

        navigator.waitPresenceOfElement(CheckoutPO.BTN_PROCEED_CHECKOUT);
        navigator.click(CheckoutPO.BTN_PROCEED_CHECKOUT);

        navigator.waitPresenceOfElement(CheckoutPO.BTN_PLACE_ORDER);
        navigator.click(CheckoutPO.BTN_PLACE_ORDER);
        navigator.closeAds();
    }

    @E("realizo o pagamento dos produtos")
    public void realizoPagamentoDosProdutos() {
        navigator.waitPresenceOfElement(CheckoutPO.LBL_NAME_CARD);

        navigator.sendKeys(CheckoutPO.LBL_NAME_CARD, "Marcos Damasco");
        navigator.sendKeys(CheckoutPO.LBL_CARD_NUMBER, "5352 0882 2689 9894");
        navigator.sendKeys(CheckoutPO.LBL_CVC, "600");
        navigator.sendKeys(CheckoutPO.LBL_EXPIRATION_MONTH, "07");
        navigator.sendKeys(CheckoutPO.LBL_EXPIRATION_YEAR, "2025");
        navigator.takeScreenShot();

        navigator.click(CheckoutPO.BTN_CONFIRM_ORDER);
    }

    @Então("valido que o pagamento foi realizado com sucesso")
    public void validoPagamentoComSucesso() {
        navigator.takeScreenShot();
        Assertions.assertEquals(true, navigator.verifyPresenceOfElement(OrderCompletePO.TXT_ORDER_PLACED), "Compra não realizada");
    }
}
