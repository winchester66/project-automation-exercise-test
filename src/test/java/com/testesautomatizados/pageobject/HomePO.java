package com.testesautomatizados.pageobject;

import org.openqa.selenium.By;

public class HomePO {

	public static By IMG_HOME = By.xpath("//img[@alt='Website for automation practice']");
	public static By BTN_LOGIN_SINGUP = By.xpath("//a[contains(@href,'login')]");
	public static By LBL_VALIDATION_LOGIN = By.xpath("//a[contains(text(),'Logged in as')]");
	public static By BTN_CONTINUE_SHOPPING = By.xpath("//button[contains(text(),'Continue Shopping')]");
	public static By LNK_VIEW_CART = By.xpath("//li/a[contains(@href,'view_cart')]");
}
