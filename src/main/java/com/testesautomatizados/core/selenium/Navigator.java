package com.testesautomatizados.core.selenium;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.testesautomatizados.core.util.ScenarioUtil;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Navigator {

    private static Map<String, WebDriver> drivers = new HashMap<String, WebDriver>();

    public static void startBrowser(String url) throws MalformedURLException {

        WebDriver driver = null;
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-dev-shm-usage");

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(chromeOptions);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.navigate().to(url);
        drivers.put(ScenarioUtil.getScenario(), driver);
    }

    public static WebDriver getDriver() {
        return drivers.get(ScenarioUtil.getScenario());
    }

    public static void closeDriver() {
        WebDriver driver = drivers.get(ScenarioUtil.getScenario());
        if (driver != null) {
            driver.close();
        }
        drivers.remove(ScenarioUtil.getScenario());
    }

    public static void takeScreenshot() {

        WebDriver driver = getDriver();
        if (driver != null) {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            ScenarioUtil.getCurrentScenario().attach(screenshot, "image/png", "Evidência");
        }
    }
}

