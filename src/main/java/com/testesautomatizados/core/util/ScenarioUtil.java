package com.testesautomatizados.core.util;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class ScenarioUtil {
	
	   private static Scenario test;

	@Before
	    public void before(Scenario scenario) {
	        setScenario(scenario);
	    }
	
	public static String getScenario() {
		return test.getName();
	}
	
	public static Scenario getCurrentScenario() {
		return test;
	}

	public static void setScenario(Scenario scenario) {
		test = scenario;
	}
}
