package com.testesautomatizados.core.util;

import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {
	
	public static String getPropertie(String prop) {

		Properties prop1 = new Properties();
		
		try {
		    prop1.load(PropertiesUtil.class.getClassLoader().getResourceAsStream("selenium.properties"));
		
		    return prop1.getProperty(prop);
		
		} 
		catch (IOException ex) {
		    ex.printStackTrace();
		}
		return null;
	}
}