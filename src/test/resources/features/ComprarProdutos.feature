# language: pt
# Author: Carlos Moreira
# Date: 13/07/2023

@all @comprarProdutos
Funcionalidade: Compra de Produtos

  Contexto: Realizar login
    Dado que estou na pagina principal
    Quando realizo login com email "marcos@mail.com" e senha "Teste123"
    Então valido que login foi realizado com sucesso

  Cenário: Comprar produtos
    Quando adiciono os produtos no carrinho
      | Stylish Dress                             | 3 |
      | Beautiful Peacock Blue Cotton Linen Saree | 2 |
      | Men Tshirt                                | 1 |
    E sigo para a tela de pagamento
    E realizo o pagamento dos produtos
    Então valido que o pagamento foi realizado com sucesso


